# Larry-UI

If you encounter problems please take a look into the [FAQ](https://gitlab.com/larry.robotics/larry.robotics/blob/master/FAQ.md).

## Using Larry-UI
To start the Larry-UI switch into the larry-ui folder and type
```
# ./build/larry-ui
```

The first thing you see is the connection window where you have to enter the ip address
of larry. After you have connected to larry you can drive him by using the keys 
```w```, ```a```, ```s```, ```d```. To rotate the camera use ```q```, ```e```, ```r```, ```f```.
If you want to play with Larry's LED lights use ```z```, ```x```, ```c```. The ```v``` key changes
toggles the LED blinking mode.

### Without Larry
To use the Larry-UI for testing purposes only you can compile larry-services
with gpio and librealsense support disabled by setting the cmake switches `-DENABLE_GPIO_SUPPORT=OFF` and 
`-DENABLE_LIBREALSENSE_SUPPORT=OFF` like
```sh
cd larry-services
cmake -Bbuild -H. -DCMAKE_BUILD_TYPE=Release -DENABLE_GPIO_SUPPORT=OFF -DENABLE_LIBREALSENSE_SUPPORT=OFF
cd build
make
...
```
Afterwards you can start all services with the execution manager
```sh
./build/services/ServiceStarter
```
The Intel Realsense Tracking Camera T265 or the Intel Realsense Stereo Camera d435 will be used
if you plug them into your usb port before starting all services. 

Now you can use the Larry-UI by connecting to ```127.0.0.1```. Every service should work
and the gpio based services should give you a response on the console when they receive
a command like driving or camera rotation.

### With Larry
Compile larry-services with gpio support enabled and start the execution manager `./build/services/ServiceStarter` 
on larry like it is described in the [larry-services Readme](https://gitlab.com/el.chris/larry-services/README.md).

After that you can start the Larry-UI and can connect to Larry. If you accessed the Larry-WLan 
Larry's ip address will be ```10.0.0.1```

## Building the remote Larry User Interface

### Prerequisites
The Larry UI uses cmake as a build system and you are required to have 
```cmake >= 3.13``` installed!
Additionally, Larry UI uses 3Delch has 3D engine and user interface which has 
the following package requirements:
 * cmake >= 3.13
 * lua >= 5.2
 * assimp
 * glew
 * glm
 * sdl2 >= 2.0.5
 * sdl2_image
 * sdl2_ttf
 * googletest
 * googlemock

 You have to install them before you can start building the Larry UI. 

#### Ubuntu
```
apt install cmake libassimp-dev liblua5.3-dev libglew-dev libglm-dev libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libacl1-dev
```

### 3rd Party Prerequisites
Additionally,
you need access to the 3rd party dependencies of 3Delch which are containing fonts,
textures, models etc. To get them just type
```sh
cd larry-ui
git clone https://gitlab.com/el.chris/3delch-3rd-party.git
mv 3delch-3rd-party data
```
The 3rd party dependencies have to be stored under ```data``` hence we moved them
as a last step.

### Building Larry-UI
```sh
cmake -Bbuild -H. -DCMAKE_BUILD_TYPE=Release
cd build
make -j16 larry-ui
```

### Running Larry-UI
Since larry-ui is looking for the configuration under ```cfg``` and needs to have
all 3rd party data files under ```data``` you have to go back into larry-ui repository
root directory and run it from there.
```sh
./build/larry-ui
```
To connect to larry the larry-services must be up and running on larry!
