#include "World/World.hpp"

#include "common/ServiceDescriptionGenerator.hpp"

using namespace el3D;
using namespace common;

namespace World
{
World::World( _3Delch::elScene& scene ) noexcept
    : ObjectFactory( scene ), scene{ &scene },
      coordinateSystem( { { 20, 20 },
                          HighGL::elCoordinateSystem::GridStyle::Lines,
                          coordinateSystemPosition } ),
      robot( scene )
{
    this->directionalLight = this->scene->CreateDirectionalLight(
        glm::vec3( 0.9f, 0.9f, 0.95f ), glm::vec3( -1.0f, 0.5f, 0.0f ), 0.6f,
        0.3f );
    this->directionalLight->SetCastShadow( true );

    this->scene->SetupColoredSkybox( glm::vec4( 0.0f ) );
    this->scene->AddObjects< OpenGL::RenderTarget::Forward >(
        this->coordinateSystem.GetMajorMinorDotAndLineObjects() );
    this->scene->AddCustomObject< OpenGL::RenderTarget::Deferred >(
        this->coordinateSystem.GetPlaneObject() );
}

World::~World()
{
    this->scene->RemoveObjects< OpenGL::RenderTarget::Forward >(
        this->coordinateSystem.GetMajorMinorDotAndLineObjects() );
    this->scene->RemoveCustomObject< OpenGL::RenderTarget::Deferred >(
        this->coordinateSystem.GetPlaneObject() );
    this->scene->RemoveDirectionalLight( this->directionalLight );
}

void
World::Update() noexcept
{
    this->robot.Update();
}

void
World::Connect( const std::string& domain, const std::string& entity ) noexcept
{
    this->robot.Connect( domain, entity );
}

void
World::Disconnect() noexcept
{
    this->robot.Disconnect();
}


} // namespace World
