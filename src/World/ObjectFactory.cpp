#include "World/ObjectFactory.hpp"

#include "buildingBlocks/algorithm_extended.hpp"

using namespace el3D;

namespace World
{
ObjectFactory::ObjectFactory( el3D::_3Delch::elScene &scene ) noexcept
    : scene{ &scene }
{
}

void
ObjectFactory::RemoveObject( void *rawObjectPtr ) noexcept
{
    auto iter = bb::find_if(
        this->objects, [=]( auto &ptr ) { return ptr.get() == rawObjectPtr; } );

    if ( iter != this->objects.end() ) this->objects.erase( iter );
}

void
ObjectFactory::UpdateChildren() noexcept
{
    for ( auto &object : this->objects )
        object->Update();
}

void
ObjectFactory::ConnectChildren( const std::string &domain,
                                const std::string &entity ) noexcept
{
    for ( auto &object : this->objects )
        object->Connect( domain, entity );
}

void
ObjectFactory::DisconnectChildren() noexcept
{
    for ( auto &object : this->objects )
        object->Disconnect();
}


} // namespace World
