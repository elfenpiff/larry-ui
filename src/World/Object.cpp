#include "World/Object.hpp"

namespace World
{
Object::Object( el3D::_3Delch::elScene& scene, const ObjectType type ) noexcept
    : type{ type }, scene{ &scene }
{
}

void
Object::Update() noexcept
{
    this->UpdateImplementation();
}

void
Object::Connect( const std::string& domain, const std::string& entity ) noexcept
{
    this->ConnectImplementation( domain, entity );
}

void
Object::Disconnect() noexcept
{
    this->DisconnectImplementation();
}

el3D::_3Delch::elScene&
Object::GetScene() noexcept
{
    return *this->scene;
}


} // namespace World
