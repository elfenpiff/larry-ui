#include "LarryScanner/LarryScanner.hpp"

#include "3Delch/3Delch.hpp"
#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Entry.hpp"
#include "GuiGL/elWidget_InfoMessage.hpp"
#include "GuiGL/elWidget_Listbox.hpp"
#include "GuiGL/elWidget_Window.hpp"
#include "common/ServiceDescriptionGenerator.hpp"
#include "common/Settings.hpp"
#include "iceoryx_posh/runtime/posh_runtime.hpp"

using namespace el3D;
using namespace common;

namespace LarryScanner
{
LarryScanner::LarryScanner(
    el3D::GuiGL::elWidget_base* const parent,
    const connectCallback_t&          connectCallback,
    const disconnectCallback_t&       disconnectCallback ) noexcept
    : parent( parent ),
      window( parent->Add< GuiGL::elWidget_base >( "LarryScanner" ) ),
      preview( parent, window.Get() ), connectCallback( connectCallback ),
      disconnectCallback( disconnectCallback )
{
    this->entityEntry = this->window->Add< GuiGL::elWidget_Entry >();
    this->entityEntry->SetPosition( { ELEMENT_SPACING, ELEMENT_SPACING } );
    this->entityEntry->SetText( "larry@larry_robotics" );
    this->entityEntry->SetSize( { this->startWindowSize.x - 2 * ELEMENT_SPACING,
                                  this->entityEntry->GetSize().y } );
    this->entityEntry->SetOnEnterKeyPressCallback(
        [&] { this->ConnectButtonCallback( ConnectionType::DirectConnect ); } );

    this->directConnectButton = this->window->Add< GuiGL::elWidget_Button >();
    this->directConnectButton->SetPosition(
        { ELEMENT_SPACING, this->entityEntry->GetPosition().y +
                               this->entityEntry->GetSize().y +
                               ELEMENT_SPACING } );
    this->directConnectButton->SetText( "direct connect" );
    this->directConnectButton->SetSize(
        { this->startWindowSize.x - 2 * ELEMENT_SPACING,
          this->entityEntry->GetSize().y } );
    this->directConnectButton->SetClickCallback(
        [=] { this->ConnectButtonCallback( ConnectionType::DirectConnect ); } );

    this->domainLabel = this->window->Add< GuiGL::elWidget_Label >();
    this->domainLabel->SetPosition(
        { ELEMENT_SPACING, this->directConnectButton->GetPosition().y +
                               this->directConnectButton->GetSize().y +
                               4 * ELEMENT_SPACING + 5.0f } );
    this->domainLabel->SetText( "domain" );

    this->domainEntry = this->window->Add< GuiGL::elWidget_Entry >();
    this->domainEntry->SetPosition(
        { 80, this->domainLabel->GetPosition().y - 5.0f } );
    this->domainEntry->SetText( "larry_robotics" );
    this->domainEntry->SetSize( { 220, this->domainEntry->GetSize().y } );

    this->scanButton = this->window->Add< GuiGL::elWidget_Button >();
    this->scanButton->SetPosition(
        { ELEMENT_SPACING, this->domainEntry->GetSize().y +
                               this->domainEntry->GetPosition().y +
                               ELEMENT_SPACING } );
    this->scanButton->SetText( "Scan" );

    this->scanButton->SetClickCallback( [=] { this->ScanButtonCallback(); } );

    this->exitButton = this->window->Add< GuiGL::elWidget_Button >();
    this->exitButton->SetPosition(
        { 2.0f * ELEMENT_SPACING + this->scanButton->GetSize().x,
          this->scanButton->GetPosition().y } );
    this->exitButton->SetText( "Exit" );
    this->exitButton->SetClickCallback(
        [] { _3Delch::_3Delch::GetInstance()->StopMainLoopAndDestroy(); } );

    this->startWindowSize.y = this->exitButton->GetPosition().y +
                              this->exitButton->GetSize().y + ELEMENT_SPACING;

    this->window->SetSize( this->startWindowSize );

    this->servicesFound = this->window->Add< GuiGL::elWidget_Label >();
    this->servicesFound->SetPosition(
        { ELEMENT_SPACING, this->scanButton->GetSize().y + ELEMENT_SPACING +
                               this->scanButton->GetPosition().y } );
    this->servicesFound->SetDoRenderWidget( false );

    this->entities = this->window->Add< GuiGL::elWidget_Listbox >();
    this->entities->SetPosition( this->servicesFound->GetPosition() );
    this->entities->SetDoRenderWidget( false );
    this->entities->SetSize(
        { this->startWindowSize.x - 2.0f * ELEMENT_SPACING, 200.0f } );
    this->entities->SetElementSelectionCallback(
        [=] { this->SelectRobotInstanceCallback(); } );

    this->connect = this->window->Add< GuiGL::elWidget_Button >();
    this->connect->SetPosition( { this->domainEntry->GetPosition().x +
                                      this->domainEntry->GetSize().x +
                                      ELEMENT_SPACING,
                                  ELEMENT_SPACING } );
    this->connect->SetSize( { 400, this->connect->GetSize().y } );
    this->connect->SetClickCallback( [=] {
        this->ConnectButtonCallback( ConnectionType::ConnectThroughScan );
    } );

    this->disconnect = this->window->Add< GuiGL::elWidget_Button >();
    this->disconnect->SetDoRenderWidget( false );
    this->disconnect->SetClickCallback(
        [=] { this->DisconnectButtonCallback(); } );
    this->disconnect->SetSize( { 300, this->disconnect->GetSize().y } );
    this->disconnect->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_CENTER, false );

    this->serviceDetails = this->window->Add< GuiGL::elWidget_Table >();
    this->serviceDetails->SetElementBaseType( el3D::GuiGL::TYPE_LABEL );
    this->serviceDetails->SetElementDefaultDimensions( 200, 20 );
    this->serviceDetails->SetHasTitleRow( true );
    this->serviceDetails->SetOnSelectionCallback(
        [=] { this->SelectServiceCallback(); } );

    this->serviceDetails->SetHoveringMode(
        GuiGL::elWidget_Table::TABLEMODE_ROW );
    this->serviceDetails->SetSelectionMode(
        GuiGL::elWidget_Table::TABLEMODE_ROW );
    this->serviceDetails->SetPosition(
        { this->connect->GetPosition().x, this->connect->GetSize().y +
                                              this->connect->GetPosition().y +
                                              ELEMENT_SPACING } );
    this->serviceDetails->SetSize(
        { this->connect->GetSize().x,
          this->entities->GetPosition().y + this->entities->GetSize().y +
              -this->connect->GetSize().y - 2 * ELEMENT_SPACING } );
}

void
LarryScanner::DisconnectButtonCallback() noexcept
{
    if ( this->disconnectCallback ) this->disconnectCallback();
    this->availableServices.clear();

    this->ShowScannerWidgets( true );
    this->window->ResizeTo( this->startWindowSize );
    this->window->SetPositionPointOfOrigin( GuiGL::POSITION_CENTER, true );
    this->window->MoveTo( { 0, 0 } );
}

void
LarryScanner::ConnectButtonCallback(
    const ConnectionType connectionType ) noexcept
{
    switch ( connectionType )
    {
        case ConnectionType::ConnectThroughScan:
        {
            if ( this->connectCallback )
                this->connectCallback( { this->domainEntry->GetText(),
                                         this->selectedRobot,
                                         this->availableServices } );
            break;
        }
        case ConnectionType::DirectConnect:
        {
            std::string rawEntityString = this->entityEntry->GetText();
            uint64_t    pos             = rawEntityString.find_first_of( "@" );

            auto printWarning = [&] {
                GuiGL::elWidget_InfoMessage* msg =
                    this->parent->Create< GuiGL::elWidget_InfoMessage >();
                msg->SetText(
                    { "Wrong entity format!", "Please use ENTITY@DOMAIN!" } );
            };

            if ( pos == std::string::npos )
            {
                printWarning();
                return;
            }

            std::string entity = rawEntityString.substr( 0, pos );
            std::string domain = rawEntityString.substr( pos + 1 );

            if ( entity.empty() || domain.empty() )
            {
                printWarning();
                return;
            }

            this->ConnectToAnnouncementService( entity, domain, [=] {
                if ( this->connectCallback )
                    this->connectCallback(
                        { domain, entity, this->availableServices } );
            } );

            break;
        }
    }

    this->ShowScannerWidgets( false );

    this->window->ResizeTo( this->disconnect->GetSize() +
                            glm::vec2( ELEMENT_SPACING ) );
    this->window->SetPositionPointOfOrigin( GuiGL::POSITION_BOTTOM_CENTER,
                                            true );
    this->window->MoveTo( { 0, 0 } );

    this->disconnect->SetText( "disconnect" );
}

void
LarryScanner::ShowScannerWidgets( const bool v ) noexcept
{
    this->entityEntry->SetDoRenderWidget( v );
    this->directConnectButton->SetDoRenderWidget( v );
    this->domainEntry->SetDoRenderWidget( v );
    this->domainLabel->SetDoRenderWidget( v );
    this->scanButton->SetDoRenderWidget( v );
    this->exitButton->SetDoRenderWidget( v );
    this->servicesFound->SetDoRenderWidget( v );
    this->entities->SetDoRenderWidget( v );
    this->connect->SetDoRenderWidget( v );
    this->serviceDetails->SetDoRenderWidget( v );

    this->disconnect->SetDoRenderWidget( !v );

    this->preview.Reset();
}

void
LarryScanner::Update() noexcept
{
    if ( this->UpdateAvailableServices() )
        if ( this->serviceUpdateCallback ) this->serviceUpdateCallback();

    this->preview.Update();
}

bool
LarryScanner::UpdateAvailableServices() noexcept
{
    bool hasUpdatedServices{ false };
    if ( this->announcementService.has_value() &&
         this->announcementService->getSubscriptionState() ==
             iox::SubscribeState::SUBSCRIBED &&
         this->announcementService->hasSamples() )
    {
        this->announcementService->take().and_then(
            [&]( iox::popo::Sample< const serviceData::identifier_t >&
                     identifier ) {
                this->availableServices.resize(
                    identifier->availableServices.size() );

                uint64_t pos{ 0u };
                for ( auto& s : identifier->availableServices )
                    this->availableServices[pos++] = s;

                hasUpdatedServices = true;
            } );
    }

    if ( hasUpdatedServices ) this->announcementService.reset();
    return hasUpdatedServices;
}

void
LarryScanner::SelectRobotInstanceCallback() noexcept
{
    this->selectedRobot.clear();
    auto idx = this->entities->GetSelectedWidgetText();
    if ( idx.empty() ) return;
    this->selectedRobot = idx[0];

    this->connect->SetText( "connect to: " + this->selectedRobot );
    this->window->ResizeTo( this->serviceDetails->GetPosition() +
                            this->serviceDetails->GetSize() +
                            glm::vec2( ELEMENT_SPACING ) );

    this->ConnectToAnnouncementService(
        this->selectedRobot, this->domainEntry->GetText(), [&] {
            this->serviceDetails->ResetNumberOfColsAndRows(
                2, this->availableServices.size() );

            this->serviceDetails->SetColumnWidth( 0, 100 );
            this->serviceDetails->SetColumnWidth( 1, 300 );
            this->serviceDetails->SetTitleRowButtonText( 0, "state" );
            this->serviceDetails->SetTitleRowButtonText( 1, "service" );

            uint64_t pos{ 0u };
            for ( auto& s : this->availableServices )
            {
                static_cast< GuiGL::elWidget_Label* >(
                    this->serviceDetails->GetElement( 1, pos++ ) )
                    ->SetText( s.c_str() );
            }
        } );
    this->preview.Reset();
}

void
LarryScanner::ConnectToAnnouncementService(
    const std::string& entity, const std::string& domain,
    const std::function< void() >& onUpdate ) noexcept
{
    this->announcementService.emplace(
        ServiceDescriptionGenerator::ConnectToAnnouncementService( domain,
                                                                   entity ),
        iox::popo::SubscriberOptions{ 1U, 0U } );
    this->announcementService->subscribe();
    this->serviceUpdateCallback = onUpdate;
}

void
LarryScanner::ScanButtonCallback() noexcept
{
    iox::runtime::InstanceContainer results;

    iox::runtime::PoshRuntime::getInstance()
        .findService( ServiceDescriptionGenerator::FindOtherInstances(
            this->domainEntry->GetText() ) )
        .and_then( [&]( auto& result ) { results = result; } )
        .or_else( []( auto& ) {
            LOG_ERROR( 0 ) << "unable to scan for larry instances";
        } )
        .value();

    if ( results.empty() )
    {
        this->servicesFound->SetDoRenderWidget( true );
        this->entities->SetDoRenderWidget( false );
        this->servicesFound->SetText( "no services found" );
        this->window->ResizeTo(
            { this->startWindowSize.x, this->servicesFound->GetPosition().y +
                                           this->servicesFound->GetSize().y +
                                           ELEMENT_SPACING } );
    }
    else
    {
        this->entities->Clear();
        this->servicesFound->SetDoRenderWidget( false );
        this->entities->SetDoRenderWidget( true );
        this->window->ResizeTo(
            { this->startWindowSize.x, this->entities->GetPosition().y +
                                           this->entities->GetSize().y +
                                           ELEMENT_SPACING } );
        for ( auto& r : results )
            this->entities->AddElement( r.c_str(), 0 );
    }

    this->scanButton->SetText( "Rescan" );
    this->preview.Reset();
}

void
LarryScanner::SelectServiceCallback() noexcept
{
    auto selected = this->serviceDetails->GetSelection();
    if ( selected.empty() ) return;
    auto service = static_cast< GuiGL::elWidget_Label* >(
                       this->serviceDetails->GetElement( 1, selected[0].y ) )
                       ->GetText();
    if ( this->selectedRobot.empty() ) return;

    this->preview.GeneratePreview( this->domainEntry->GetText(), service,
                                   this->selectedRobot );
}
} // namespace LarryScanner
