#include "common/Helplets.hpp"

namespace common
{
std::string
DoubleToString( const double v, const uint64_t length,
                const bool addSpaceIfPositive ) noexcept
{
    std::string ret = std::to_string( v );
    auto        pos = ret.find_first_of( '.' );
    if ( pos == std::string::npos ) return ret;

    if ( ret.size() - pos >= length ) ret.resize( pos + length + 1, '0' );

    if ( addSpaceIfPositive && !ret.empty() )
        if ( ret[0] != '-' ) ret.insert( ret.begin(), ' ' );

    return ret;
}
} // namespace common
