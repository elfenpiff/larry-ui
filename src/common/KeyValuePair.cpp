#include "common/KeyValuePair.hpp"

#include "GuiGL/elWidget_Graph.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_base.hpp"

using namespace el3D;

namespace common
{
void
KeyValuePair::SetUp( el3D::GuiGL::elWidget_base* const parent,
                     const std::string& keyText, const float yPos,
                     const float xPos ) noexcept
{
    this->key = parent->Add< GuiGL::elWidget_Label >();
    this->key->SetClassName( "Key", false );
    this->key->SetText( keyText );
    this->key->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_LEFT );
    this->key->SetPosition( { ELEMENT_SPACING + xPos, yPos } );

    this->value = parent->Add< GuiGL::elWidget_Label >();
    this->value->SetClassName( "Value", false );
    this->value->SetPosition( { ELEMENT_SPACING + xPos, yPos } );
    this->value->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_RIGHT );
}

void
KeyValuePair::SetUp( el3D::GuiGL::elWidget_base* const parent,
                     const std::string&                keyText,
                     el3D::GuiGL::elWidget_base* const after ) noexcept
{
    this->SetUp( parent, keyText, after->GetSize().y + after->GetPosition().y );
}

void
KeyGraphPair::SetUp( el3D::GuiGL::elWidget_base* const parent,
                     const std::string& keyText, const float yPos,
                     const float graphWidth, const std::string& shader,
                     const std::string& shaderGroup ) noexcept
{
    this->key = parent->Add< GuiGL::elWidget_Label >();
    this->key->SetClassName( "Key", false );
    this->key->SetText( keyText );
    this->key->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_LEFT );
    this->key->SetPosition( { ELEMENT_SPACING, yPos } );

    this->label = parent->Add< GuiGL::elWidget_Label >();
    this->label->SetClassName( "Value", false );
    this->label->SetText( "" );
    this->label->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_RIGHT );
    this->label->SetPosition( { ELEMENT_SPACING * 2. + graphWidth, yPos } );

    this->graph = parent->Add< GuiGL::elWidget_Graph >();
    this->graph->SetUpGraphShader( shader, shaderGroup );
    this->graph->SetPosition( { ELEMENT_SPACING, yPos } );
    this->graph->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_RIGHT );
    this->graph->SetSize( { graphWidth, this->key->GetSize().y - 2.0 } );

    for ( uint64_t i = 0; i < this->graphData.SIZE; ++i )
        this->graphData.Push( 0.0f );
    this->graph->SetGraphData(
        this->graphData.GetContent< std::vector< float > >(), { 0.0f, 1.0f } );
}

void
KeyGraphPair::SetUp( el3D::GuiGL::elWidget_base* const parent,
                     const std::string&                keyText,
                     el3D::GuiGL::elWidget_base* const after,
                     const float graphWidth, const std::string& shader,
                     const std::string& shaderGroup ) noexcept
{
    return this->SetUp( parent, keyText,
                        after->GetPosition().y + after->GetSize().y, graphWidth,
                        shader, shaderGroup );
}

void
KeyGraphPair::AddGraphValue( const float value,
                             const bool  adjustRange ) noexcept
{
    this->graphData.Push( value );

    std::vector< float > data =
        this->graphData.GetContent< std::vector< float > >();
    std::reverse( data.begin(), data.end() );

    if ( adjustRange )
    {
        float minData = *std::min_element( data.begin(), data.end() );
        float maxData = *std::max_element( data.begin(), data.end() );
        if ( minData == maxData ) maxData += 1.0f;

        this->graph->SetGraphData( data, { minData, maxData } );
    }
    else
    {
        this->graph->SetGraphData( data, { 0.0f, 1.0f } );
    }
}


} // namespace common
