#include "Control/Control.hpp"

#include "3Delch/3Delch.hpp"
#include "GLAPI/elWindow.hpp"
#include "common/ServiceDescriptionGenerator.hpp"

using namespace common;
using namespace el3D;

namespace Control
{
Control::Control() noexcept
{
    auto eventHandler =
        el3D::_3Delch::_3Delch::GetInstance()->Window()->GetEventHandler();

    auto addDriveEvent = [=]( SDL_Keycode key, bool* state ) {
        this->events.emplace_back( eventHandler->CreateEvent() );
        this->events.back()
            ->Set(
                [=]( const SDL_Event& e ) { return e.key.keysym.sym == key; } )
            ->SetCallback(
                [=]( const SDL_Event& e ) { *state = e.type == SDL_KEYDOWN; } );
    };

    addDriveEvent( this->driveForward, &this->driveForwardPressed );
    addDriveEvent( this->driveBackward, &this->driveBackwardPressed );
    addDriveEvent( this->driveTurnLeft, &this->driveTurnLeftPressed );
    addDriveEvent( this->driveTurnRight, &this->driveTurnRightPressed );

    auto addCameraEvent = [=]( SDL_Keycode key, Direction direction ) {
        this->events.emplace_back( eventHandler->CreateEvent() );
        this->events.back()
            ->Set( [=]( const SDL_Event& e ) {
                return e.type == SDL_KEYDOWN && e.key.keysym.sym == key;
            } )
            ->SetCallback( [=]( const SDL_Event& ) {
                this->SendCameraCommand( direction );
            } );
    };

    addCameraEvent( this->cameraUp, Direction::Up );
    addCameraEvent( this->cameraDown, Direction::Down );
    addCameraEvent( this->cameraLeft, Direction::Left );
    addCameraEvent( this->cameraRight, Direction::Right );
}

Control::~Control()
{
}

void
Control::Update() noexcept
{
    if ( this->driveTurnLeftPressed )
        this->SendDriveCommand( { -100, 100 } );
    else if ( this->driveTurnRightPressed )
        this->SendDriveCommand( { 100, -100 } );
    else if ( this->driveForwardPressed )
        this->SendDriveCommand( { 100, 100 } );
    else if ( this->driveBackwardPressed )
        this->SendDriveCommand( { -100, -100 } );
    else
        this->SendDriveCommand( { 0, 0 } );
}

void
Control::SendCameraCommand( const Direction direction ) noexcept
{
    if ( !this->cameraInfo || !this->cameraCommands ) return;

    this->cameraInfo->take()->and_then(
        [&]( iox::popo::Sample< const serviceData::camera_t<> >& camera ) {
            this->currentAngleX = camera->angle.xAngle;
            this->currentAngleY = camera->angle.yAngle;
        } );

    if ( direction == Direction::Up )
        ++this->currentAngleY;
    else if ( direction == Direction::Down )
        --this->currentAngleY;
    else if ( direction == Direction::Left )
        ++this->currentAngleX;
    else if ( direction == Direction::Right )
        --this->currentAngleX;

    if ( this->cameraCommands
             ->publishCopyOf( serviceData::cameraCommand_t{
                 this->currentAngleX, this->currentAngleY } )
             .has_error() )
    {
        LOG_ERROR( 0 ) << "unable to send camera command";
        return;
    }
}

void
Control::SendDriveCommand( const serviceData::drive_t& cmd ) noexcept
{
    if ( !this->driveCommands ) return;

    if ( cmd.left == this->lastDriveCommand.left &&
         cmd.right == this->lastDriveCommand.right &&
         this->driveCommandRepetitionInterval >
             units::Time::TimeSinceEpoch() - this->lastDriveCommandUpdate )
        return;

    this->driveCommands->publishCopyOf( cmd );

    this->lastDriveCommandUpdate = units::Time::TimeSinceEpoch();
    this->lastDriveCommand       = cmd;
}

void
Control::Connect( const std::string& domain,
                  const std::string& entity ) noexcept
{
    this->driveCommands.emplace(
        ServiceDescriptionGenerator::ConnectToServiceCommand(
            { domain, "Drive", entity } ) );
    this->driveCommands->offer();

    this->cameraCommands.emplace(
        ServiceDescriptionGenerator::ConnectToServiceCommand(
            { domain, "Camera", entity } ) );
    this->cameraCommands->offer();

    this->cameraInfo.emplace( ServiceDescriptionGenerator::ConnectToServiceInfo(
                                  { domain, "Camera", entity } ),
                              iox::popo::SubscriberOptions{ 1U, 0U } );
    this->cameraInfo->subscribe();

    this->RegisterEvents();
}

void
Control::Disconnect() noexcept
{
    this->driveCommands.reset();
    this->cameraCommands.reset();
    this->cameraInfo.reset();

    this->UnregisterEvents();
}

void
Control::RegisterEvents() noexcept
{
    for ( auto e : this->events )
        e->Register();
}

void
Control::UnregisterEvents() noexcept
{
    for ( auto e : this->events )
        e->Unregister();
}

} // namespace Control
