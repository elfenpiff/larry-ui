#include "ServiceMonitor/Service_base.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Graph.hpp"
#include "GuiGL/elWidget_Label.hpp"

#include <algorithm>

using namespace el3D;
using namespace common;

namespace ServiceMonitor
{
Service_base::Service_base( const Service_CTor& args ) noexcept
    : subscriber(
          ServiceDescriptionGenerator::ConnectToServiceInfo( args.service ),
          iox::popo::SubscriberOptions{ 1U, 0U } )
{
    this->subscriber.subscribe();
    this->header    = args.header;
    this->infoFrame = args.infoFrame;
    this->infoFrame->SetSize( { this->infoFrame->GetSize().x, 0.0f } );
}

Service_base::~Service_base()
{
    if ( this->subscriber.getSubscriptionState() ==
         iox::SubscribeState::SUBSCRIBED )
        this->subscriber.unsubscribe();
}

bool
Service_base::HasServiceDescripton( const service_t& service ) const noexcept
{
    return ( ServiceDescriptionGenerator::ConnectToServiceInfo( service ) ==
             this->subscriber.getServiceDescription() );
}

bool
Service_base::IsConnected() const
{
    return this->subscriber.getSubscriptionState() ==
           iox::SubscribeState::SUBSCRIBED;
}

void
Service_base::Update() noexcept
{
}

} // namespace ServiceMonitor
