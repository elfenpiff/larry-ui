#include "ServiceMonitor/Factory.hpp"

#include "ServiceMonitor/Service_Camera.hpp"
#include "ServiceMonitor/Service_Drive.hpp"
#include "ServiceMonitor/Service_LedControl.hpp"
#include "ServiceMonitor/Service_StereoCamera.hpp"
#include "ServiceMonitor/Service_SystemMonitor.hpp"
#include "ServiceMonitor/Service_TrackingCamera.hpp"
#include "ServiceMonitor/Service_TrackingSensor.hpp"
#include "ServiceMonitor/Service_UltraSonicSensor.hpp"

namespace ServiceMonitor
{
std::unique_ptr< Service_base >
Factory::Create( const Service_CTor& ctor ) noexcept
{
    if ( ctor.service.service == "Camera" )
        return std::unique_ptr< Service_base >( new Service_Camera( ctor ) );
    else if ( ctor.service.service == "Drive" )
        return std::unique_ptr< Service_base >( new Service_Drive( ctor ) );
    else if ( ctor.service.service == "LedControl" )
        return std::unique_ptr< Service_base >(
            new Service_LedControl( ctor ) );
    else if ( ctor.service.service == "StereoCamera" )
        return std::unique_ptr< Service_base >(
            new Service_StereoCamera( ctor ) );
    else if ( ctor.service.service == "SystemMonitor" )
        return std::unique_ptr< Service_base >(
            new Service_SystemMonitor( ctor ) );
    else if ( ctor.service.service == "TrackingCamera" )
        return std::unique_ptr< Service_base >(
            new Service_TrackingCamera( ctor ) );
    else if ( ctor.service.service == "TrackingSensor" )
        return std::unique_ptr< Service_base >(
            new Service_TrackingSensor( ctor ) );
    else if ( ctor.service.service == "UltraSonicSensor" )
        return std::unique_ptr< Service_base >(
            new Service_UltraSonicSensor( ctor ) );
    else
        return std::unique_ptr< Service_base >( new Service_base( ctor ) );
}
} // namespace ServiceMonitor
