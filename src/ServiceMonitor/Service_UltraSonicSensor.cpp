#include "ServiceMonitor/Service_UltraSonicSensor.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "common/Helplets.hpp"
#include "common/Settings.hpp"
#include "serviceData/ultraSonicSensor_t.hpp"

using namespace el3D;

namespace ServiceMonitor
{
Service_UltraSonicSensor::Service_UltraSonicSensor( const Service_CTor& args )
    : Service_base( args )
{
    this->sensorData = this->header->Add< GuiGL::elWidget_Label >();
    this->sensorData->SetClassName( "Value", false );
    this->sensorData->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_RIGHT );
    this->sensorData->SetText( "0.0 m" );

    this->distance.SetUp( this->infoFrame, "distance",
                          common::ELEMENT_SPACING );
    this->distance.value->SetText( "0.0 m" );

    this->angle.SetUp( this->infoFrame, "angle",
                       this->distance.key->GetSize().y +
                           this->distance.key->GetPosition().y );
    this->angle.value->SetText( "0.00" );

    this->infoFrame->SetSize( { 0, this->angle.key->GetSize().y +
                                       this->angle.key->GetPosition().y +
                                       common::ELEMENT_SPACING } );
}

void
Service_UltraSonicSensor::Update() noexcept
{
    Service_base::Update();

    if ( this->IsConnected() && this->subscriber.hasSamples() )
    {
        this->subscriber.take()->and_then(
            [&]( iox::popo::Sample< const void >& chunk ) {
                const serviceData::ultraSonicSensor_t* d =
                    reinterpret_cast< const serviceData::ultraSonicSensor_t* >(
                        chunk.get() );

                std::string distanceString =
                    ( d->distance.GetMeter() ==
                      std::numeric_limits< bb::float_t< 64 > >::max() )
                        ? "infinite"
                        : d->distance.ToString( 1 );

                this->distance.value->SetText( distanceString );
                this->sensorData->SetText( distanceString );

                this->angle.value->SetText( d->angle.ToString( 1 ) );
            } );
    }
}
} // namespace ServiceMonitor
