#include "ServiceMonitor/Service_Camera.hpp"

#include "GLAPI/elImage.hpp"
#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Image.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "common/Helplets.hpp"
#include "common/Settings.hpp"
#include "serviceData/camera_t.hpp"

using namespace el3D;

namespace ServiceMonitor
{
Service_Camera::Service_Camera( const Service_CTor& args ) noexcept
    : Service_base( args )
{
    this->frameCounterLabel =
        this->header->Add< GuiGL::elWidget_Label >( "Value" );
    this->frameCounterLabel->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_RIGHT );
    this->frameCounterLabel->SetText( "0" );

    this->cameraImage = this->infoFrame->Add< GuiGL::elWidget_Image >();
    this->cameraImage->SetSize( { this->infoFrame->GetSize().x,
                                  this->infoFrame->GetSize().x / 16 * 9 } );

    this->resolution.SetUp( this->infoFrame, "resolution",
                            this->cameraImage->GetSize().y +
                                common::ELEMENT_SPACING );
    this->resolution.value->SetText( "0 x 0" );

    this->angle.SetUp( this->infoFrame, "angle",
                       this->resolution.key->GetSize().y +
                           this->resolution.key->GetPosition().y );
    this->angle.value->SetText( "[ 0.00, 0.00 ]" );

    this->imageSize.SetUp( this->infoFrame, "size",
                           this->angle.key->GetSize().y +
                               this->angle.key->GetPosition().y );
    this->imageSize.value->SetText( "0 b" );

    this->fps.SetUp( this->infoFrame, "fps",
                     this->imageSize.key->GetSize().y +
                         this->imageSize.key->GetPosition().y );
    this->fps.value->SetText( "0" );

    this->infoFrame->SetSize( { 0, this->fps.key->GetSize().y +
                                       this->fps.key->GetPosition().y +
                                       common::ELEMENT_SPACING } );
}

void
Service_Camera::Update() noexcept
{
    Service_base::Update();

    if ( this->IsConnected() && this->subscriber.hasSamples() )
    {
        this->subscriber.take().and_then( [&]( iox::popo::Sample< const void >&
                                                   chunk ) {
            const serviceData::camera_t<>* c =
                static_cast< const serviceData::camera_t<>* >( chunk.get() );

            if ( c->width > 0u && c->height > 0u )
            {
                if ( !this->cameraImageTexture.has_value() ||
                     ( this->cameraImageTexture->GetSize().x != c->width ||
                       this->cameraImageTexture->GetSize().y != c->height ) )
                {
                    this->cameraImageTexture.emplace( "", c->width, c->height );
                    this->cameraImage->SetImageFromTexture(
                        &*this->cameraImageTexture );
                }

                auto image = GLAPI::elImage::Create(
                    const_cast< uint8_t* >( c->data ), c->dataSize,
                    SDL_PIXELFORMAT_RGBA32 );
                if ( !image.HasError() )
                    this->cameraImageTexture->TexImage(
                        ( *image )->GetBytePointer() );
            }

            this->resolution.value->SetText( std::to_string( c->width ) +
                                             " x " +
                                             std::to_string( c->height ) );
            this->angle.value->SetText(
                "[ " + common::DoubleToString( c->angle.xAngle ) + ", " +
                common::DoubleToString( c->angle.yAngle ) + " ]" );
            this->imageSize.value->SetText( std::to_string( c->dataSize ) +
                                            " b" );
            this->fps.value->SetText(
                std::to_string( this->frameCounter.GetThroughputPerSecond() ) );

            this->frameCounterLabel->SetText(
                std::to_string( this->frameCounter.GetThroughputPerSecond() ) );
            this->frameCounter.IncrementWith( 1u );
        } );
    }
}
} // namespace ServiceMonitor
