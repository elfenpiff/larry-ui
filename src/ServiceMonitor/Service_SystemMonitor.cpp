#include "ServiceMonitor/Service_SystemMonitor.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Graph.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_Window.hpp"
#include "buildingBlocks/algorithm_extended.hpp"
#include "common/Helplets.hpp"
#include "serviceData/systemMonitor_t.hpp"
#include "units/DataSize.hpp"
#include "units/Frequency.hpp"

using namespace el3D;

namespace ServiceMonitor
{
Service_SystemMonitor::Service_SystemMonitor( const Service_CTor& args )
    : Service_base( args )
{
    this->headerCPULoad = this->header->Add< GuiGL::elWidget_Label >();
    this->headerCPULoad->SetClassName( "Value", false );
    this->headerCPULoad->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_RIGHT );
    this->headerCPULoad->SetText( common::DoubleToString( 0.0 ) );

    this->embeddedOverview.frame =
        this->infoFrame->Add< GuiGL::elWidget_Window >();
    this->embeddedOverview.frame->SetStickToParentSize( { 0, 0 } );
    this->embeddedOverview.frame->SetIsMovable( false );
    this->embeddedOverview.frame->SetIsResizable( false );
    this->embeddedOverview.frame->SetBorderSize( { 0, 0, 0, 0 } );
    this->embeddedOverview.frame->SetHasVerticalScrolling( true );
    this->embeddedOverview.frame->SetIsScrollbarEnabled( false, false, false,
                                                         false );
    for ( uint64_t i = GuiGL::widgetStates_t::STATE_DEFAULT;
          i < GuiGL::widgetStates_t::STATE_END; ++i )
    {
        this->embeddedOverview.frame->SetBaseColorForState(
            { 0, 0, 0, 0 }, static_cast< GuiGL::widgetStates_t >( i ) );
        this->embeddedOverview.frame->SetAccentColorForState(
            { 0, 0, 0, 0 }, static_cast< GuiGL::widgetStates_t >( i ) );
    }
}

void
Service_SystemMonitor::Update() noexcept
{
    Service_base::Update();

    if ( this->IsConnected() && this->subscriber.hasSamples() )
    {
        this->subscriber.take().and_then( [&]( iox::popo::Sample< const void >&
                                                   chunk ) {
            const serviceData::systemInfo_t* sysInfo =
                reinterpret_cast< const serviceData::systemInfo_t* >(
                    chunk.get() );
            this->headerCPULoad->SetText(
                common::DoubleToString( sysInfo->cpu.load / 100.0 ) );

            this->UpdateDetailedView( this->embeddedOverview, sysInfo );
            this->infoFrame->SetSize(
                { this->infoFrame->GetSize().x,
                  std::min(
                      SYSTEM_MONITOR_MAX_HEIGHT,
                      this->embeddedOverview.cpuLoad.key->GetPosition().y +
                          this->embeddedOverview.cpuLoad.key->GetSize().y ) } );
        } );
    }
}

void
Service_SystemMonitor::SetUpDetailedView(
    detail_t& view, const serviceData::systemInfo_t* const sysInfo ) noexcept
{
    if ( view.isSetUp ) return;

    view.uptime.SetUp( view.frame.Get(), "Uptime", common::ELEMENT_SPACING );
    view.uptime.value->SetText( "0" );
    view.hostname.SetUp( view.frame.Get(), "Hostname", view.uptime.key.Get() );
    view.kernel.SetUp( view.frame.Get(), "Kernel", view.hostname.key.Get() );

    view.netHeader = view.frame->Add< el3D::GuiGL::elWidget_Label >();
    view.netHeader->SetClassName( "Section", false );
    view.netHeader->SetPosition(
        { common::ELEMENT_SPACING / 2., view.kernel.key->GetSize().y +
                                            view.kernel.key->GetPosition().y +
                                            common::ELEMENT_SPACING } );
    view.netHeader->SetText( "Network" );

    /// Network
    for ( uint64_t i = 0; i < sysInfo->network.interfaces.size(); ++i )
    {
        auto next = ( view.net.empty() ) ? view.netHeader.Get()
                                         : view.net.back().outgoing.key.Get();

        auto& dev = sysInfo->network.interfaces[i];
        // ignore loopback
        if ( std::string( dev.interfaceName.c_str() ) == "lo" ) continue;
        view.net.emplace_back();
        auto& n = view.net.back();

        n.device.SetUp( view.frame.Get(), dev.interfaceName,
                        next->GetSize().y + next->GetPosition().y +
                            common::ELEMENT_SPACING );
        n.device.value->SetText( dev.ipAddress );
        n.incoming.SetUp( view.frame.Get(), "incoming", n.device.key.Get(),
                          GRAPH_ENTRY_WIDTH / 3 * 2, GRAPH_SHADER_FILE, "cpu" );
        n.outgoing.SetUp( view.frame.Get(), "outgoing", n.incoming.key.Get(),
                          GRAPH_ENTRY_WIDTH / 3 * 2, GRAPH_SHADER_FILE, "cpu" );
    }

    /// Memory
    auto next = ( view.net.empty() ) ? view.kernel.key.Get()
                                     : view.net.back().outgoing.key.Get();

    view.memoryUsage.SetUp( view.frame.Get(), "Memory",
                            next->GetSize().y + next->GetPosition().y +
                                common::ELEMENT_SPACING,
                            GRAPH_SECTION_WIDTH, GRAPH_SHADER_FILE, "cpu" );
    view.memoryUsage.key->SetClassName( "Section", false );
    view.memoryUsage.key->SetPosition(
        { common::ELEMENT_SPACING / 2.,
          view.memoryUsage.key->GetPosition().y } );

    view.memoryTotal.SetUp( view.frame.Get(), "total",
                            view.memoryUsage.key.Get() );
    view.memoryUsed.SetUp( view.frame.Get(), "used",
                           view.memoryTotal.key.Get() );
    view.memoryCached.SetUp( view.frame.Get(), "cached",
                             view.memoryUsed.key.Get() );
    view.memoryBuffered.SetUp( view.frame.Get(), "buffered",
                               view.memoryCached.key.Get() );

    /// CPU
    next = view.memoryBuffered.key.Get();

    view.cpuLoad.SetUp( view.frame.Get(), "CPU",
                        next->GetSize().y + next->GetPosition().y +
                            common::ELEMENT_SPACING,
                        GRAPH_SECTION_WIDTH, GRAPH_SHADER_FILE, "cpu" );
    view.cpuLoad.key->SetClassName( "Section", false );
    view.cpuLoad.key->SetPosition(
        { common::ELEMENT_SPACING / 2., view.cpuLoad.key->GetPosition().y } );

    for ( uint64_t i = 0; i < sysInfo->cpu.numberOfCores; ++i )
    {
        view.cpuCoreLoad.emplace_back();
        view.cpuCoreLoad.back().SetUp(
            view.frame.Get(), "core " + std::to_string( i ),
            ( i == 0 ) ? view.cpuLoad.key.Get()
                       : view.cpuCoreLoad[i - 1].key.Get(),
            GRAPH_ENTRY_WIDTH, GRAPH_SHADER_FILE, "cpu" );
    }

    view.isSetUp = true;
}

void
Service_SystemMonitor::UpdateDetailedView(
    detail_t& view, const serviceData::systemInfo_t* const sysInfo ) noexcept
{
    this->SetUpDetailedView( view, sysInfo );

    view.uptime.value->SetText( std::to_string( sysInfo->uptime ) );
    view.hostname.value->SetText( sysInfo->hostname );
    view.kernel.value->SetText( sysInfo->kernelVersion );

    for ( uint64_t i = 0; i < sysInfo->network.interfaces.size(); ++i )
    {
        auto& dev = sysInfo->network.interfaces[i];
        // ignore loopback
        if ( std::string( dev.interfaceName.c_str() ) == "lo" ) continue;

        auto iter = bb::find_if( view.net, [&]( auto& n ) {
            return n.device.key->GetText() == dev.interfaceName;
        } );

        if ( iter == view.net.end() ) continue;

        iter->device.value->SetText( dev.ipAddress );

        iter->incoming.AddGraphValue(
            static_cast< float >( dev.incoming.byteLoad ), true );
        iter->incoming.label->SetText(
            units::DataSize::Bytes(
                static_cast< float >( dev.incoming.byteLoad ) )
                .ToString( 1 ) +
            "/s" );

        iter->outgoing.AddGraphValue(
            static_cast< float >( dev.outgoing.byteLoad ), true );
        iter->outgoing.label->SetText(
            units::DataSize::Bytes(
                static_cast< float >( dev.outgoing.byteLoad ) )
                .ToString( 2 ) +
            "/s" );
    }

    view.cpuLoad.AddGraphValue(
        static_cast< float >( sysInfo->cpu.load / 100. ) );
    view.cpuLoad.label->SetText(
        common::DoubleToString( sysInfo->cpu.load / 100. ) );

    for ( uint64_t i = 0, limit = sysInfo->cpu.cores.size();
          i < sysInfo->cpu.numberOfCores && i < limit; ++i )
    {
        view.cpuCoreLoad[i].AddGraphValue(
            static_cast< float >( sysInfo->cpu.cores[i].load / 100. ) );
        view.cpuCoreLoad[i].label->SetText(
            units::Frequency::MHz( sysInfo->cpu.cores[i].frequencyInMhz )
                .ToString( 2 ) );
    }

    view.memoryUsage.AddGraphValue(
        static_cast< float >( sysInfo->memory.used ) /
        static_cast< float >( sysInfo->memory.total ) );
    view.memoryUsage.label->SetText( common::DoubleToString(
        static_cast< float >( sysInfo->memory.used ) /
        static_cast< float >( sysInfo->memory.total ) ) );
    view.memoryTotal.value->SetText(
        el3D::units::DataSize::Bytes(
            static_cast< double >( sysInfo->memory.total ) )
            .ToString( 2 ) );
    view.memoryUsed.value->SetText(
        el3D::units::DataSize::Bytes(
            static_cast< double >( sysInfo->memory.used ) )
            .ToString( 2 ) );
    view.memoryCached.value->SetText(
        el3D::units::DataSize::Bytes(
            static_cast< double >( sysInfo->memory.cached ) )
            .ToString( 2 ) );
    view.memoryBuffered.value->SetText(
        el3D::units::DataSize::Bytes(
            static_cast< double >( sysInfo->memory.buffer ) )
            .ToString( 2 ) );
}


} // namespace ServiceMonitor
