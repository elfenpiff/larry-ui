#include "ServiceMonitor/ServiceMonitor.hpp"

#include "GuiGL/elWidget_Titlebar.hpp"
#include "ServiceMonitor/Factory.hpp"
#include "buildingBlocks/algorithm_extended.hpp"

#include <memory>

using namespace el3D;

namespace ServiceMonitor
{
ServiceMonitor::ServiceMonitor( el3D::GuiGL::elWidget_base* const parent,
                                const std::string&                domain,
                                const std::string& instance ) noexcept
    : domain( domain ), instance( instance )
{
    this->window = parent->Add< GuiGL::elWidget_Window >();
    this->window->SetClassName( "ServiceMonitor", true );
    this->window->SetStickToParentSize( { -1.0, 0.0 } );
    auto size = this->window->GetSize();
    this->window->SetSize( { 0, 0 } );
    this->window->ResizeTo( size );
}

void
ServiceMonitor::AddService( const std::string& service ) noexcept
{
    std::unique_ptr< ServiceEntry_t > newService( new ServiceEntry_t );
    newService->header = this->window->Add< el3D::GuiGL::elWidget_Button >();
    newService->header->SetStickToParentSize( { 0, -1 } );
    newService->header->SetSize( { 0, HEADER_HEIGHT } );
    newService->header->SetText( service );
    newService->header->SetTextAlignment(
        el3D::GuiGL::positionCorner_t::POSITION_TOP_LEFT, { 5, 5 } );

    newService->infoFrame =
        newService->header->Add< el3D::GuiGL::elWidget_base >();
    newService->infoFrame->SetClassName( "ServiceMonitorInfoFrame", false );
    newService->infoFrame->SetStickToParentSize( { 0, -1 } );
    newService->infoFrame->SetPosition( { 0, HEADER_HEIGHT } );
    newService->infoFrame->SetSize( { this->window->GetSize().x, 0 } );

    if ( !this->services.empty() )
    {
        newService->previousHeader = this->services.back()->header.Get();
        newService->header->SetStickToWidget(
            newService->previousHeader, el3D::GuiGL::VPOSITION_BOTTOM,
            el3D::GuiGL::HPOSITION_LEFT_ALIGNED );
    }
    else
        newService->header->SetPosition( { 0, 0 } );

    // set expand callback
    newService->header->SetClickCallback( [newService = newService.get()] {
        newService->header->ResizeTo(
            { 0, HEADER_HEIGHT + ( ( !newService->isOpen )
                                       ? newService->infoFrame->GetSize().y
                                       : 0 ) } );
        newService->isOpen = !newService->isOpen;
    } );

    newService->service = Factory::Create(
        Service_CTor{ { this->domain, service, this->instance },
                      newService->header.Get(),
                      newService->infoFrame.Get() } );
    this->services.emplace_back( std::move( newService ) );

    this->Reshape();
}

void
ServiceMonitor::RemoveService( const std::string& service ) noexcept
{
    auto iter = bb::find_if( this->services, [&]( auto& e ) {
        return e->service->HasServiceDescripton(
            { this->domain, service, this->instance } );
    } );
    if ( iter == this->services.end() ) return;

    this->services.erase( iter );

    if ( !this->services.empty() )
    {
        this->services.front()->previousHeader = nullptr;
        for ( uint64_t i = 1, limit = this->services.size(); i < limit; ++i )
            this->services[i]->previousHeader =
                this->services[i - 1]->header.Get();
    }

    this->Reshape();
}

void
ServiceMonitor::Update() noexcept
{
    for ( auto& e : this->services )
        e->service->Update();
}

void
ServiceMonitor::Reshape() noexcept
{
    float currentHeight = 0;
    for ( auto& e : this->services )
    {
        e->header->SetPosition( { 0, currentHeight } );
        currentHeight += e->header->GetSize().y;
    }
}

} // namespace ServiceMonitor

