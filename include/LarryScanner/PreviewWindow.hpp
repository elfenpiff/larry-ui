#pragma once

#include "GuiGL/GuiGL.hpp"
#include "ServiceMonitor/Service_base.hpp"

#include <glm/glm.hpp>

namespace LarryScanner
{
class PreviewWindow
{
  public:
    PreviewWindow(
        el3D::GuiGL::elWidget_base* const parent,
        el3D::GuiGL::elWidget_base* const stickyWindowSource ) noexcept;

    void
    Update() noexcept;
    void
    Reset( const std::string& message = "",
           const std::string& detail  = "" ) noexcept;

    void
    GeneratePreview( const std::string& domain, const std::string& service,
                     const std::string& entity ) noexcept;

  private:
    void
    ResizeWindow( const float height ) noexcept;
    float
    GetRequiredHeaderHeight() const noexcept;

  private:
    el3D::GuiGL::elWidget_base*     parent{ nullptr };
    el3D::GuiGL::elWidget_base*     stickyWindowSource{ nullptr };
    el3D::GuiGL::elWidget_base_ptr  window;
    el3D::GuiGL::elWidget_Label_ptr header;
    el3D::GuiGL::elWidget_Label_ptr headerDetail;
    el3D::GuiGL::elWidget_base_ptr  content;
    std::unique_ptr< ServiceMonitor::Service_base > previewService;
    float                                           previewHeight{ 0.0f };
    glm::vec4                                       borderSize;
};
} // namespace LarryScanner
