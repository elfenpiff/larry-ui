#pragma once

#include "ServiceMonitor/Service_base.hpp"
#include "common/KeyValuePair.hpp"

namespace ServiceMonitor
{
class Service_TrackingCamera : public Service_base
{
  public:
    Service_TrackingCamera( const Service_CTor& args ) noexcept;
    void
    Update() noexcept override;

  private:
    el3D::GuiGL::elWidget_Label_ptr distanceFromOrigin;

    static constexpr uint64_t VALUE_ACCURACY = 1u;

    common::KeyValuePair position;
    common::KeyValuePair rotation;
    common::KeyValuePair velocity;
    common::KeyValuePair acceleration;
    common::KeyValuePair angularVelocity;
    common::KeyValuePair angularAcceleration;

    common::KeyValuePair trackerConfidence;
    common::KeyValuePair mapperConfidence;
};
} // namespace ServiceMonitor
