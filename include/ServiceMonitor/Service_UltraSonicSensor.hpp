#pragma once

#include "ServiceMonitor/Service_base.hpp"
#include "common/KeyValuePair.hpp"

namespace ServiceMonitor
{
class Service_UltraSonicSensor : public Service_base
{
  public:
    Service_UltraSonicSensor( const Service_CTor& args );
    void
    Update() noexcept override;

  private:
    el3D::GuiGL::elWidget_Label_ptr sensorData;
    common::KeyValuePair            distance;
    common::KeyValuePair            angle;
};
} // namespace ServiceMonitor
