#pragma once

#include "ServiceMonitor/Service_base.hpp"

namespace ServiceMonitor
{
class Service_LedControl : public Service_base
{
  public:
    Service_LedControl( const Service_CTor& args );
    void
    Update() noexcept override;

  private:
    el3D::GuiGL::elWidget_Label_ptr lights;
};

} // namespace ServiceMonitor
