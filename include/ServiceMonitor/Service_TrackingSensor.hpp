#pragma once

#include "ServiceMonitor/Service_base.hpp"

namespace ServiceMonitor
{
class Service_TrackingSensor : public Service_base
{
  public:
    Service_TrackingSensor( const Service_CTor& args ) noexcept;
    void
    Update() noexcept override;

  private:
    el3D::GuiGL::elWidget_Label_ptr state;
};
} // namespace ServiceMonitor
