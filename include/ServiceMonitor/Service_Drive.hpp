#pragma once

#include "ServiceMonitor/Service_base.hpp"

namespace ServiceMonitor
{
class Service_Drive : public Service_base
{
  public:
    Service_Drive( const Service_CTor& args );
    void
    Update() noexcept override;

  private:
    el3D::GuiGL::elWidget_Label_ptr currentSpeed;
};
} // namespace ServiceMonitor
