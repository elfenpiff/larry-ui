#pragma once

#include "ServiceMonitor/Service_base.hpp"
#include "common/KeyValuePair.hpp"

namespace serviceData
{
struct systemInfo_t;
}

namespace ServiceMonitor
{
class Service_SystemMonitor : public Service_base
{
  public:
    Service_SystemMonitor( const Service_CTor& args );

    void
    Update() noexcept override;

  protected:
    static constexpr char     GRAPH_SHADER_FILE[] = "glsl/sysInfo_Graph.glsl";
    static constexpr uint64_t GRAPH_SECTION_WIDTH = 175;
    static constexpr uint64_t GRAPH_ENTRY_WIDTH   = 125;
    static constexpr float    SYSTEM_MONITOR_MAX_HEIGHT = 800.0f;

    struct network_t
    {
        common::KeyValuePair device;
        common::KeyGraphPair incoming;
        common::KeyGraphPair outgoing;
    };

    struct detail_t
    {
        el3D::GuiGL::elWidget_Window_ptr    frame;
        bool                                isSetUp{ false };
        common::KeyValuePair                uptime;
        common::KeyValuePair                hostname;
        common::KeyValuePair                kernel;
        el3D::GuiGL::elWidget_Label_ptr     netHeader;
        std::vector< network_t >            net;
        common::KeyGraphPair                cpuLoad;
        std::vector< common::KeyGraphPair > cpuCoreLoad;
        common::KeyGraphPair                memoryUsage;
        common::KeyValuePair                memoryTotal;
        common::KeyValuePair                memoryUsed;
        common::KeyValuePair                memoryCached;
        common::KeyValuePair                memoryBuffered;
    };

    void
    SetUpDetailedView(
        detail_t&                              view,
        const serviceData::systemInfo_t* const sysInfo ) noexcept;

    void
    UpdateDetailedView(
        detail_t&                              view,
        const serviceData::systemInfo_t* const sysInfo ) noexcept;

  protected:
    el3D::GuiGL::elWidget_Label_ptr headerCPULoad;
    detail_t                        embeddedOverview;
};
} // namespace ServiceMonitor
