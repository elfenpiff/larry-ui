#pragma once

#include "3Delch/elScene.hpp"
#include "HighGL/elCoordinateSystem.hpp"
#include "HighGL/elLight_DirectionalLight.hpp"
#include "OpenGL/elGeometricObject_InstancedObject.hpp"
#include "World/ObjectFactory.hpp"
#include "World/Vehicle.hpp"

namespace World
{
class World : public ObjectFactory
{
  public:
    World( el3D::_3Delch::elScene& scene ) noexcept;
    ~World();

    void
    Update() noexcept;
    void
    Connect( const std::string& domain, const std::string& entity ) noexcept;
    void
    Disconnect() noexcept;

  private:
    el3D::_3Delch::elScene*                 scene       = nullptr;
    glm::vec4                               skyboxColor = glm::vec4( 0.0f );
    el3D::HighGL::elLight_DirectionalLight* directionalLight = nullptr;

    glm::vec3 coordinateSystemPosition = glm::vec3( -10.0, -10.0, 0.0 );
    el3D::HighGL::elCoordinateSystem coordinateSystem;

    Vehicle robot;
};
} // namespace World
