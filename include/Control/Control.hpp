#pragma once

#include "GLAPI/elEvent.hpp"
#include "iceoryx_posh/popo/publisher.hpp"
#include "iceoryx_posh/popo/subscriber.hpp"
#include "serviceData/camera_t.hpp"
#include "serviceData/drive_t.hpp"
#include "units/Time.hpp"

#include <optional>
#include <vector>

namespace Control
{
class Control
{
  public:
    Control() noexcept;

    // delete all copy/move stuff
    ~Control();

    void
    Connect( const std::string& domain, const std::string& entity ) noexcept;
    void
    Disconnect() noexcept;
    void
    Update() noexcept;

  private:
    enum class Direction
    {
        Up,
        Down,
        Left,
        Right
    };

  private:
    void
    RegisterEvents() noexcept;
    void
    UnregisterEvents() noexcept;
    void
    SendDriveCommand( const serviceData::drive_t& cmd ) noexcept;
    void
    SendCameraCommand( const Direction direction ) noexcept;

  private:
    std::vector< el3D::GLAPI::elEvent* > events;

    uint16_t    currentAngleX = 0u;
    uint16_t    currentAngleY = 0u;
    SDL_Keycode cameraUp      = SDLK_r;
    SDL_Keycode cameraDown    = SDLK_f;
    SDL_Keycode cameraLeft    = SDLK_q;
    SDL_Keycode cameraRight   = SDLK_e;
    std::optional< iox::popo::TypedPublisher< serviceData::cameraCommand_t > >
        cameraCommands;
    std::optional< iox::popo::TypedSubscriber< serviceData::camera_t<> > >
        cameraInfo;


    SDL_Keycode          driveForward          = SDLK_w;
    SDL_Keycode          driveBackward         = SDLK_s;
    SDL_Keycode          driveTurnLeft         = SDLK_a;
    SDL_Keycode          driveTurnRight        = SDLK_d;
    bool                 driveForwardPressed   = false;
    bool                 driveBackwardPressed  = false;
    bool                 driveTurnLeftPressed  = false;
    bool                 driveTurnRightPressed = false;
    serviceData::drive_t lastDriveCommand{ 0, 0 };
    el3D::units::Time    driveCommandRepetitionInterval =
        el3D::units::Time::MilliSeconds( 50 );
    el3D::units::Time lastDriveCommandUpdate;
    std::optional< iox::popo::TypedPublisher< serviceData::drive_t > >
        driveCommands;
};
} // namespace Control
