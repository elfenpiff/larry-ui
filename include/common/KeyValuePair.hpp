#pragma once

#include "GuiGL/GuiGL.hpp"
#include "buildingBlocks/elCircularBuffer.hpp"
#include "common/Settings.hpp"

#include <cstdint>

namespace common
{
struct KeyGraphPair
{
    using graphData_t = el3D::bb::elCircularBuffer< float, GRAPH_DATA_SIZE >;

    void
    SetUp( el3D::GuiGL::elWidget_base* const parent, const std::string& keyText,
           const float yPos, const float graphWidth, const std::string& shader,
           const std::string& shaderGroup ) noexcept;
    void
    SetUp( el3D::GuiGL::elWidget_base* const parent, const std::string& keyText,
           el3D::GuiGL::elWidget_base* const after, const float graphWidth,
           const std::string& shader, const std::string& shaderGroup ) noexcept;
    void
    AddGraphValue( const float value, const bool adjustRange = false ) noexcept;

    el3D::GuiGL::elWidget_Label_ptr key;
    el3D::GuiGL::elWidget_Graph_ptr graph;
    el3D::GuiGL::elWidget_Label_ptr label;
    graphData_t                     graphData;
};

struct KeyValuePair
{
    void
    SetUp( el3D::GuiGL::elWidget_base* const parent, const std::string& keyText,
           const float yPos, const float xPos = 0.0f ) noexcept;
    void
    SetUp( el3D::GuiGL::elWidget_base* const parent, const std::string& keyText,
           el3D::GuiGL::elWidget_base* const after ) noexcept;

    el3D::GuiGL::elWidget_Label_ptr key;
    el3D::GuiGL::elWidget_Label_ptr value;
};


} // namespace common
