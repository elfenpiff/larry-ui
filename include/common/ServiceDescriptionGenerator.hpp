#pragma once

#include "iceoryx_posh/capro/service_description.hpp"

namespace common
{
struct service_t
{
    std::string domain;
    std::string service;
    std::string entity;
};

using ServiceDescription = iox::capro::ServiceDescription;

struct ServiceDescriptionGenerator
{

    static constexpr char EVENT_ANNOUNCE[] = "announce";
    static constexpr char EVENT_INFO[]     = "info";
    static constexpr char EVENT_COMMAND[]  = "command";
    static constexpr char ENTRY_SEPARATOR  = '_';

    static ServiceDescription
    FindOtherInstances( const std::string& domain ) noexcept;

    static ServiceDescription
    ConnectToAnnouncementService( const std::string& domain,
                                  const std::string& entity ) noexcept;

    static ServiceDescription
    ConnectToServiceInfo( const service_t& service ) noexcept;
    static ServiceDescription
    ConnectToServiceCommand( const service_t& service ) noexcept;
};
} // namespace common
