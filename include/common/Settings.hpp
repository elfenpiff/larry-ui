#pragma once

#include <cstdint>

namespace common
{

static constexpr float    ELEMENT_SPACING  = 10.0f;
static constexpr uint64_t GRAPH_DATA_SIZE  = 50;
static constexpr char     EVENT_ANNOUNCE[] = "announce";

} // namespace common
