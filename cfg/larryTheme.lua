TRUE = 1
FALSE = 0
-- if no value is set the default is always 0

function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

base = {}

mouseCursor = {
    animationSpeed = 1.0,              -- mouse cursor movement speed

    size = {36, 36},
    animationShaderFile = "glsl/gui/shaderTexture_MouseCursor.glsl",

    default = {0.2, 0.2, 0.2, 1.0,
                0.6, 0.6, 0.6, 1.0},
    borderSize = {0, 0, 0, 0},
}

label = {
    default = {0.8, 0.9, 1.0, 0.0,
                   0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
    inactive = {0.8, 0.9, 1.0, 0.0,
                   0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
    hover = {0.8, 0.9, 1.0, 0.0,
                   0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
    clicked = {0.8, 0.9, 1.0, 0.0,
                   0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
    selected = {0.8, 0.9, 1.0, 0.0,
                   0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
    borderSize = {0, 0, 0, 0},
    isResizable = FALSE,
    isMovable = FALSE,
    isUnSelectable = TRUE,
    disableEventHandling = TRUE,
    fontFile = "data/fonts/Terminus.ttf",
    fontSize = 14
}

keyValuePair = {
    isResizable = FALSE,
    isMovable = FALSE,
    disableEventHandling = TRUE,

    label_Key = {
        default     = {0.9, 0.9, 0.9, 0.0, 0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
        inactive    = {0.9, 0.9, 0.9, 0.0, 0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
        hover       = {0.9, 0.9, 0.9, 0.0, 0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
        clicked     = {0.9, 0.9, 0.9, 0.0, 0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
        selected    = {0.9, 0.9, 0.9, 0.0, 0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
        borderSize = {0, 0, 0, 0},
        isResizable = FALSE,
        isMovable = FALSE,
        disableEventHandling = TRUE,
        fontFile = "data/fonts/Terminus.ttf",
        fontSize = 16,
    },
    label_Value = {
        default     = {0.6, 1.0, 0.6, 0.0, 0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
        inactive    = {0.6, 1.0, 0.6, 0.0, 0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
        hover       = {0.6, 1.0, 0.6, 0.0, 0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
        clicked     = {0.6, 1.0, 0.6, 0.0, 0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
        selected    = {0.6, 1.0, 0.6, 0.0, 0.0, 0.0, 0.0, 0.0}, -- alpha value MUST always be zero
        borderSize = {0, 0, 0, 0},
        isResizable = FALSE,
        isMovable = FALSE,
        disableEventHandling = TRUE,
        fontFile = "data/fonts/Terminus.ttf",
        fontSize = 16,
    },
}

button = {
    default = {0.0, 0.0, 0.0, 0.3,      -- default color
               0.9, 0.6, 0.2, 0.4},     -- accent color
    inactive = {0.2, 0.2, 0.2, 0.3,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    hover =    {0.1, 0.05, 0.0, 0.3,      -- default color
                0.9, 0.6, 0.2, 0.6},     -- accent color
    clicked =  {0.3, 0.2, 0.0, 0.05,      -- default color
                0.9, 0.6, 0.2, 1.0},     -- accent color
    selected = {0.3, 0.2, 0.0, 0.05,      -- default color
                0.9, 0.6, 0.2, 0.8},     -- accent color

    size = {140, 30},
    borderSize = {0, 0, 3, 3},

    isResizable = FALSE,
    isUnSelectable = TRUE,
    isMovable = FALSE,

    label = deepcopy(label),
}
button.label.default = {0.9, 0.8, 0.6, 0.0,
                        0.0, 0.0, 0.0, 0.0}
button.label.fontSize = 18

button_NoHighlight = deepcopy(button)
button_NoHighlight.default      = {0.2, 0.2, 0.2, 1.0, 0.0, 0.0, 0.0, 0.0}
button_NoHighlight.inactive     = {0.05, 0.05, 0.05, 1.0, 0.0, 0.0, 0.0, 0.0}
button_NoHighlight.hover        = {0.22, 0.22, 0.22, 1.0, 0.0, 0.0, 0.0, 0.0}
button_NoHighlight.clicked      = {0.25, 0.25, 0.25, 1.0, 0.0, 0.0, 0.0, 0.0}
button_NoHighlight.selected     = {0.25, 0.25, 0.25, 1.0, 0.0, 0.0, 0.0, 0.0}


window = {
    default = {0.0, 0.0, 0.03, 0.6,      -- default color
               0.0, 0.8, 1.0, 0.4},     -- accent color
    inactive = {0.1, 0.1, 0.1, 0.4,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    hover =    {0.0, 0.0, 0.03, 0.6,      -- default color
                0.0, 0.8, 1.0, 0.8},     -- accent color
    clicked =  {0.0, 0.03, 0.03, 0.6,      -- default color
                0.0, 0.8, 1.0, 1.0},     -- accent color
    selected = {0.0, 0.0, 0.03, 0.6,      -- default color
                0.0, 0.8, 1.0, 0.8},     -- accent color
    size = {500, 500},
    borderSize = {2, 2, 0, 0},
    borderEventIncrease = {5, 5, 5, 5},
    scrollbarWidth = {10, 10, 10, 10},
    scrollbarEnabled = {0, 1, 0, 1},

    isResizable = TRUE,
    isMovable = TRUE,
}

base_IntegratedConsole = deepcopy(window)
base_IntegratedConsole.size = {1200, 500}
base_IntegratedConsole.isMovable = FALSE
base_IntegratedConsole.isResizable = FALSE
base_IntegratedConsole.isAlwaysOnTop = TRUE
base_IntegratedConsole.positionPointOfOrigin = 5 -- TOP_CENTER

dropButton  = deepcopy(button)

checkButton = {
    default = {0.0, 0.0, 0.0, 0.0,      -- default color
               0.0, 0.0, 0.0, 0.0},     -- accent color
    size = {50, 20},
    borderSize = {0, 0, 0, 0},

    animationShaderFile = "glsl/gui/shaderTexture_CheckButton.glsl",
    isUnSelectable = TRUE,
    isResizable = FALSE,
    isMovable = FALSE,
    shaderAnimation = {
        default = {0.2, 0.2, 0.2, 1.0,
                    0.0, 0.0, 0.0, 0.1},

        inactive = {0.05, 0.05, 0.05, 1.0,
                    0.0, 0.0, 0.0, 0.1},

        hover =   {0.22, 0.22, 0.22, 1.0,
                   0.0, 0.0, 0.0, 0.1},

        clicked = {0.25, 0.25, 0.25, 1.0,
                   0.0, 0.0, 0.0, 0.1},

        selected = {0.25, 0.25, 0.25, 1.0,
                    0.9, 0.8, 0.6, 0.1},
    }
}

slider = {
    button = {
        default     = button.default,
        inactive    = button.inactive,
        hover       = button.hover,
        clicked     = button.clicked,
        selected    = button.selected,
        borderSize  = {0, 0, 0, 0},
    },

    default     = {0.0, 0.0, 0.0, 0.1,
                    0.0, 0.0, 0.0, 0.1},
    inactive     = {0.0, 0.0, 0.0, 0.1,
                    0.0, 0.0, 0.0, 0.1},
    hover     = {0.0, 0.0, 0.0, 0.2,
                    0.0, 0.0, 0.0, 0.2},
    clicked     = {0.0, 0.0, 0.0, 0.25,
                    0.0, 0.0, 0.0, 0.25},
    selected     = {0.0, 0.0, 0.0, 0.25,
                    0.0, 0.0, 0.0, 0.25},

    size = {100, 40},
    swapColor = TRUE,

    isResizable = FALSE,
    isMovable = FALSE,
}

table = {
    default =   {0.0, 0.0, 0.0, 0.2,
                 0.0, 0.8, 1.0, 0.4},
    inactive =  {0.0, 0.0, 0.0, 0.2,
                 0.0, 0.0, 0.0, 0.0},
    hover =     {0.0, 0.0, 0.0, 0.2,
                 0.0, 0.8, 1.0, 0.8},
    clicked =   {0.0, 0.0, 0.0, 0.2,
                 0.0, 0.8, 1.0, 1.0},
    selected =  {0.0, 0.0, 0.0, 0.2,
                 0.0, 0.8, 1.0, 0.8},

    size = {500, 500},
    borderSize = {0, 0, 2, 2},
    scrollbarWidth = window.scrollbarWidth,
    scrollbarEnabled = window.scrollbarEnabled,

    isResizable = FALSE,
    isMovable = FALSE,
    button = deepcopy(button),

    base_HighlightCurrentRow = {
        default     = {0.0, 0.0, 0.0, 0.0, 1.0, 0.8, 0.2, 0.6},
        inactive    = {0.0, 0.0, 0.0, 0.0, 1.0, 0.8, 0.2, 0.6},
        hover       = {0.0, 0.0, 0.0, 0.0, 1.0, 0.8, 0.2, 0.6},
        clicked     = {0.0, 0.0, 0.0, 0.0, 1.0, 0.8, 0.2, 0.8},
        selected    = {0.0, 0.0, 0.0, 0.0, 1.0, 0.8, 0.2, 1.0},     
        borderSize  = {1, 1, 0, 0},

        isResizable = FALSE,
        isMovable = FALSE,
        isUnSelectable = FALSE
    },

    base_SelectCurrentRow = {
        default     = {0.0, 0.0, 0.0, 0.0, 0.0, 0.8, 1.0, 0.8},
        inactive    = {0.0, 0.0, 0.0, 0.0, 0.0, 0.8, 1.0, 0.8},
        hover       = {0.0, 0.0, 0.0, 0.0, 0.0, 0.8, 1.0, 0.8},
        clicked     = {0.0, 0.0, 0.0, 0.0, 0.0, 0.8, 1.0, 0.8},
        selected    = {0.0, 0.0, 0.0, 0.0, 0.0, 0.8, 1.0, 0.8},     
        borderSize  = {1, 1, 0, 0},

        isResizable = FALSE,
        isMovable = FALSE,
        isUnSelectable = FALSE
    },
    --base_HighlightCurrentCol = {
    --    default  = {1.0, 0.8, 0.2, 0.6, 0.0, 0.0, 0.0, 0.0},
    --    selected = {1.0, 0.8, 0.2, 1.0, 0.0, 0.0, 0.0, 0.0},     

    --    isResizable = FALSE,
    --    isMovable = FALSE,
    --    isUnSelectable = FALSE
    --},
    --base_HighlightCurrentEntry = {
    --    default  = {1.0, 0.8, 0.2, 0.6, 0.0, 0.0, 0.0, 0.0},
    --    selected = {1.0, 0.8, 0.2, 1.0, 0.0, 0.0, 0.0, 0.0},     

    --    isResizable = FALSE,
    --    isMovable = FALSE,
    --    isUnSelectable = FALSE
    --},
}
table.button.borderSize = {0, 2, 0, 0}

listbox = {
    default     = table.default,
    inactive    = table.inactive,
    hover       = table.hover,
    clicked     = table.clicked,
    selected    = table.selected,

    borderSize = table.borderSize,
    scrollbarWidth = table.scrollbarWidth,
    scrollbarEnabled = { 0, 0, 0, 1},

    size = {500, 500},

    isResizable = FALSE,
    isMovable = FALSE,
    hasMultiSelection = FALSE,

    button = deepcopy(button),
}
listbox.button.borderSize = {1, 1, 0, 0}

entry = {
    default = {0.1, 0.1, 0.1, 0.8,   -- default color
                0.1, 0.9, 0.4, 0.4},    -- accent color

    inactive = {0.1, 0.1, 0.1, 0.5,
                0.1, 0.9, 0.4, 0.0},    -- accent color

    hover =   {0.1, 0.1, 0.1, 0.8,
                0.1, 0.9, 0.4, 0.6},    -- accent color

    clicked = {0.1, 0.1, 0.1, 0.8,
                0.1, 0.9, 0.4, 1.0},    -- accent color

    selected = {0.1, 0.1, 0.1, 0.8,
                0.1, 0.9, 0.4, 0.8},    -- accent color

    size = {200, 30},
    borderSize = {1, 1, 1, 1},

    isResizable = FALSE,
    isMovable = FALSE,
    doCopySelectedTextToClipboard = FALSE,
}

textCursor = {
    default = {1.0, 1.0, 1.0, 0.2,      -- default color
               0.0, 0.0, 0.0, 0.0},     -- accent color
    inactive = {0.0, 0.0, 0.0, 0.0,      -- default color
                1.0, 1.0, 1.0, 0.2},     -- accent color
    hover = {1.0, 1.0, 1.0, 0.2,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    clicked = {1.0, 1.0, 1.0, 0.2,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    selected = {1.0, 1.0, 1.0, 0.1,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    isResizable = FALSE,
    isMovable = FALSE,
    isAlwaysOnBottom = TRUE,
    disableEventHandling = TRUE,
    borderSize = { 1, 1, 1, 1},
    animationSpeed = 10.0,
}

selection = {
    default = {1.0, 1.0, 1.0, 0.1,      -- default color
               1.0, 1.0, 1.0, 0.2},     -- accent color
    inactive = {0.0, 0.0, 0.0, 0.0,      -- default color
                1.0, 1.0, 1.0, 0.1},     -- accent color
    hover = {1.0, 1.0, 1.0, 0.1,      -- default color
                1.0, 1.0, 1.0, 0.2},     -- accent color
    clicked = {1.0, 1.0, 1.0, 0.1,      -- default color
                1.0, 1.0, 1.0, 0.2},     -- accent color
    selected = {1.0, 1.0, 1.0, 0.05,      -- default color
                1.0, 1.0, 1.0, 0.1},     -- accent color
    isResizable = FALSE,
    isMovable = FALSE,
    isAlwaysOnBottom = TRUE,
    disableEventHandling = TRUE,
    borderSize = { 1, 1, 1, 1},
    animationSpeed = 10.0,
}

textbox = {
    default  = entry.default,
    inactive = entry.inactive,
    hover    = entry.hover,
    clicked  = entry.clicked,
    selected = entry.selected,

    size = {500, 500},
    borderSize = entry.borderSize,
    scrollbarWidth = window.scrollbarWidth,
    scrollbarEnabled = window.scrollbarEnabled,

    isResizable = FALSE,
    isMovable = FALSE,
}

titlebar = {
    default = {0.0, 0.0, 0.03, 0.6,      -- default color
               0.0, 0.8, 1.0, 0.0},     -- accent color
    inactive = {0.1, 0.1, 0.1, 0.4,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    hover =    {0.0, 0.0, 0.03, 0.6,      -- default color
                0.0, 0.8, 1.0, 0.0},     -- accent color
    clicked =  {0.0, 0.03, 0.03, 0.6,      -- default color
                0.0, 0.8, 1.0, 0.0},     -- accent color
    selected = {0.0, 0.0, 0.03, 0.6,      -- default color
                0.0, 0.8, 1.0, 0.0},     -- accent color

    size = {0, 35},
    stickToParentSize = {0.0, -1.0},
    position = {0, 0},
    borderSize = {2, 2, 2, 2},

    label_title = {
        default     = label.default,
        inactive    = label.inactive,
        hover       = label.hover,
        clicked     = label.clicked,
        selected    = label.selected,
        borderSize  = {0, 0, 0, 0},
        isResizable = FALSE,
        isMovable   = FALSE,
        isUnSelectable = TRUE,
        disableEventHandling = TRUE,
        fontFile    = "data/fonts/Terminus.ttf",
        fontSize    = 16,
    },

    isResizable = FALSE,
    isMovable = FALSE,
    isAlwaysOnTop = TRUE,
    hasFixedPosition = {TRUE, TRUE},
    forwardMoveEventToParent = TRUE,

    button_close = {
        default = {0.3, 0.15, 0.15, 1.0,
                    0.9, 0.8, 0.2, 0.2},

        inactive = {0.05, 0.05, 0.05, 1.0,
                    0.8, 0.8, 0.8, 0.2},

        hover =   {0.32, 0.17, 0.17, 1.0,
                   0.9, 0.8, 0.2, 0.7},

        clicked = {0.35, 0.20, 0.20, 1.0,
                   0.9, 0.8, 0.2, 0.8},

        selected = {0.35, 0.20, 0.20, 1.0,
                    0.9, 0.8, 0.2, 0.8},
        size                = {35, 25},
        borderSize          = {0, 0, 0, 0},
        isResizable         = FALSE,
        isUnSelectable      = TRUE,
        isMovable           = FALSE,
        label               = button.label,
        stickToParentSize   = {-1.0, 0.0},
    },
}

infoMessage = {
    default = {0.0, 0.0, 0.03, 0.8,      -- default color
               0.0, 0.8, 1.0, 0.4},     -- accent color
    inactive = {0.1, 0.1, 0.1, 0.4,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    hover =    {0.0, 0.0, 0.03, 0.8,      -- default color
                0.0, 0.8, 1.0, 0.8},     -- accent color
    clicked =  {0.0, 0.03, 0.03, 0.9,      -- default color
                0.0, 0.8, 1.0, 1.0},     -- accent color
    selected = {0.0, 0.0, 0.03, 0.9,      -- default color
                0.0, 0.8, 1.0, 0.8},     -- accent color

    glowDefault =    {0.0, 0.0, 0.0, 0.0,      -- default color
                0.0, 0.8, 1.0, 0.6},     -- accent color
    glowInactive = {0.0, 0.0, 0.0, 0.0,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    glowHover =    {0.0, 0.0, 0.0, 0.0,      -- default color
                0.0, 0.8, 1.0, 0.8},     -- accent color
    glowClicked =  {0.0, 0.0, 0.0, 0.0,      -- default color
                0.0, 0.8, 1.0, 1.0},     -- accent color
    glowSelected = {0.0, 0.0, 0.0, 0.0,      -- default color
                0.0, 0.8, 1.0, 0.8},     -- accent color


    size = {280, 80},
    borderSize = {2, 2, 2, 2},
    scrollbarEnabled = {0, 0, 0, 0},
    positionPointOfOrigin = 4, -- CENTER

    isResizable = FALSE,
    isMovable = TRUE,

    textbox = {},
}

yesNoDialog = deepcopy(infoMessage)

tabbedWindow = deepcopy(window)
tabbedWindow.button_tabs = deepcopy(button)
tabbedWindow.button_tabs.borderSize = {0, 2, 0, 0}

menu = {
    borderSize = {1, 1, 1, 1},

    isResizable = FALSE,
    isMovable = FALSE,

    button = {
        default = {0.6, 0.6, 0.6, 1.0,
                    0.0, 0.0, 0.0, 0.0},

        inactive = {0.4, 0.4, 0.4, 1.0,
                    0.0, 0.0, 0.0, 0.0},

        hover =   {0.67, 0.67, 0.67, 1.0,
                   0.0, 0.0, 0.0, 0.0},

        clicked = {0.7, 0.7, 0.7, 1.0,
                   0.0, 0.0, 0.0, 0.0},

        selected = {0.7, 0.7, 0.7, 1.0,
                    0.0, 0.0, 0.0, 0.0},

        size = {200, 30},
        borderSize = {0, 1, 0, 1},

        isResizable = FALSE,
        isUnSelectable = TRUE,
        isMovable = FALSE,
        
        label = deepcopy(label)
    }
}
menu.button.label.default = {0.1, 0.1, 0.1, 0.0,
                      0.0, 0.0, 0.0, 0.0}

menuBar = deepcopy(menu)
popupMenu = deepcopy(menu)

tree = {
    default     = window.default,
    inactive    = window.inactive,
    hover       = window.hover,
    clicked     = window.clicked,
    selected    = window.selected,

    size = {500, 200},
    borderSize = window.borderSize,
    scrollbarWidth = window.scrollbarWidth,
    scrollbarEnabled = {0, 0, 0, 1},

    isResizable = FALSE,
    isMovable = FALSE,

    base_tree = {
        default     = window.default,
        inactive    = window.inactive,
        hover       = window.hover,
        clicked     = window.clicked,
        selected    = window.selected,

        borderSize = {0, 0, 2, 0},
    },

    button_treeEntry = {
        default     = button_NoHighlight.default,
        inactive    = button_NoHighlight.inactive,
        hover       = button_NoHighlight.hover,
        clicked     = button_NoHighlight.clicked,
        selected    = button_NoHighlight.selected,

        --borderSize = {0, 0, 0, 0},
        borderSize = {0, 0, 8, 8},

        isResizable = FALSE,
        isMovable = FALSE,
        isUnSelectable = TRUE,
        doCopySelectedTextToClipboard = FALSE,
    },

    button_subTree = {
        default     = button.default,
        inactive    = button.inactive,
        hover       = button.hover,
        clicked     = button.clicked,
        selected    = button.selected,

        borderSize = {0, 0, 8, 8},

        isResizable = FALSE,
        isMovable = FALSE,
        isUnSelectable = TRUE,
        doCopySelectedTextToClipboard = FALSE,
    },
}

fileBrowser = {
    default     = window.default,
    inactive    = window.inactive,
    hover       = window.hover,
    clicked     = window.clicked,
    selected    = window.selected,

    size = {1000, 600},
    borderSize = window.borderSize,  
    scrollbarWidth = window.scrollbarWidth,
    scrollbarEnabled = {0, 0, 0, 0},
 
    isResizable = TRUE,
    isMovable = TRUE,
    hasTitlebar = TRUE,
    button = deepcopy(button_NoHighlight),    
    table = deepcopy(table)
}
fileBrowser.button.borderSize = {1, 1, 1, 1}
fileBrowser.table.hasTitleRow = TRUE

image = deepcopy(window)
image.borderSize = {0, 0, 0, 0}

progress = {
    size = {200, 40},
   
    animationShaderFile = "glsl/gui/shaderTexture_ProgressBar.glsl",
    isResizable = FALSE,
    isMovable = FALSE,
    shaderAnimation = {
        default = {0.3, 0.4, 0.5, 1.0,
                    0.7, 0.8, 0.9, 0.0},
        inactive = {0.4, 0.4, 0.4, 1.0,
                    0.8, 0.8, 0.8, 0.0},
        hover = {0.4, 0.5, 0.6, 1.0,
                    0.8, 0.9, 1.0, 0.0},
        clicked = {0.4, 0.5, 0.6, 1.0,
                    0.8, 0.9, 1.0, 0.0},
        selected = {0.4, 0.5, 0.6, 1.0,      -- default color
                    0.8, 0.9, 1.0, 1.0},     -- accent color
        isResizable = FALSE,
        isMovable = FALSE,
        disableEventHandling = TRUE,
    }
}


shaderAnimation = {
    isResizable = FALSE,
    isMovable = FALSE,
    disableEventHandling = TRUE,
}

empty = {
    inactive    = default,
    hover       = default,
    clicked     = default,
    selected    = default,
}

graph = {
    default         = {0.0, 0.0, 0.0, 0.0, 
                       0.0, 0.8, 1.0, 1.0},
    disableEventHandling = TRUE,
}

require "cfg/larryHudConfig"
