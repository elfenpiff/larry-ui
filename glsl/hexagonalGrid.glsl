#ifdef GL_ES
precision mediump float;
#endif

#extension GL_OES_standard_derivatives : enable

uniform float time;
uniform vec2 mouse;
uniform vec2 resolution;

float HexagonalDistance(vec2 position) {
    position = abs(position);
    float hex = dot(position, normalize(vec2(1.0, 1.73)));
    hex = max(hex, position.x);
    return hex;
}

vec4 HexagonalGrid(vec2 uv) {
    vec2 ratio = vec2(1.0, 1.73);
    vec2 ratioHalf = ratio / 2.0;
    vec2 gridA = mod(uv, ratio) - ratioHalf;
    vec2 gridB = mod(uv - ratioHalf, ratio) - ratioHalf;
    vec2 grid = dot(gridA, gridA) < dot(gridB, gridB) ? gridA : gridB;
    
    vec2 id = uv - grid;
    vec2 coordinates;
    coordinates.x = atan(grid.x, grid.y); // polar coordinates
    coordinates.y = 0.5 - HexagonalDistance(grid); // y = distance to the edge
    return vec4(coordinates.x, coordinates.y, id.x, id.y);
}

void main( void ) {
	vec2 uv = ( gl_FragCoord.xy - 0.5 * resolution.xy ) / resolution.y;
        float cellAmount = 10.0;
	uv *= cellAmount;
	vec3 color = vec3(0.0);
	
	// color += step(HexagonalDistance(uv), 0.1); // draw simple hexagon
	// color += HexagonalDistance(uv);
	
	// color = HexagonalGrid(uv).xy;

	vec4 hc = HexagonalGrid(uv);
	//float c = smoothstep(.01, .02, hc.y);
	float c = smoothstep(.01, .02, hc.y * sin(hc.z * hc.w * time));
	
	color += c;
	
	gl_FragColor = vec4(color.xyz, 1.0);

}
