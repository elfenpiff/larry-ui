//__default__
//!fragment
#version 440

//<<common.glsl

in vec2 _texCoord;
layout(location = 0) out vec4 fragColor;

void main()
{
    vec2 uv = _texCoord.xy * setting.resolution.xy / setting.resolution.x - vec2(0.5);	
    float glow = distance(uv * 0.5, vec2(0.0, 0.2));
    
    vec2 grid = fract(uv * 30.0) - vec2(0.5);	
    grid = grid * grid;
    float lines = smoothstep(0.23, 0.25, grid.x) * smoothstep(0.0, 0.9, grid.y) 
                + smoothstep(0.23, 0.25, grid.y) * smoothstep(0.0, 0.9, grid.x);
    
    float color = glow - 0.3 * lines;

    fragColor = vec4(color, color, color, 1.0);
} 